# README #

### Project ###

* framework - laravelPHP 5
* PHP version - 7.1^
* database - mySql

### Get Start ###
* Create .env file ( can use .env-example )
* Create database ( using .env data or no)
* Run - composer install
* Run - php artisan migrate
* Run - php artisan key:generate
* For test access your localhost/

### Functions ###
* list users  
* create user 
* update user 
* delete user 

### For postman test ###

* GET FILTERS: ?id=int ?name=string, ?email= string ORDER BY WITH: ?idOrder = "DESC"||"ASC", ?nameOrder = "DESC"||"ASC", ?emailOrder = "DESC"||"ASC"
* POST localhost://api/user PARAMS name, email, password --- form-data
* PUT URL localhost://api/user/{id} --- x-www-form-urlencoded
* DELETE URL localhost://api/user/{id}

### Run tests ###

* use php artisan tinker to open test console
* run $test = new Tests\Feature\User();
* run $test->test();

### Folders description ###

* App content application
    * Bases content bases classes
    * Console is for make cron jobs
    * Contracts content is all interfaces
    * Exceptions for is handles of errors
    * HTTP content http controllers, middleware, and requests validations
    * Models represents database table or connection
    * Observes to listen events ( in this case listen only database events "insert, update, delete" )
    * Services content is application business ( controllers now is only one io )
    * Repositories to access models or external integrations ( represents the data )
* Bootstrap is a laravel settings folder
* Config application settings
* Database all database migrations, factories
* Public all transpalete archives if use webpack or front-end assets
* Resourses
    * js - angular, react, vue components
    * lang laravel  translate library
    * sass - styles
    * views front-end laravel templates
    * js and sass is transpale using webpack
* Routes application endpoints
* Storage
    * app, public is the resources/views.blade transpalete
    * framework for use exclusive of laravel
    * logs 
* Tests 
    * feature content scripts for new features test
    * unit content scripts for unit tests 
* Vendor dependencies archives

### Contact - Wilber da Silva Chaves Costa ###

* phone - (11) 98789-5615
* email - wilberchaves@gmail.com
* linkedin - https://www.linkedin.com/in/wilber-silva-010a3336/