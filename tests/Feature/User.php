<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class User extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test(){
        $this->get("/api/user")->assertStatus(HTTP_STATUS_SUCCESS);
        $this->post("/api/user");
        $this->put("/api/user/1");
        $this->delete("/api/user/1");
    }
}
