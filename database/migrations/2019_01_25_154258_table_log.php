<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableLog extends Migration{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('general_log', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('owner_id');
            $table->string('origin_table', 60);
            $table->string('owner_account_type', 10);
            $table->string('type_operation', 10);
            $table->text("log");
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('general_log');
    }
}
