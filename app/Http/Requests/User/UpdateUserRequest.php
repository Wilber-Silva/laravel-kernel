<?php
/**
 * Created by PhpStorm.
 * User: wilbe_thaae2h
 * Date: 25/01/2019
 * Time: 13:14
 */

namespace App\Http\Requests\User;

use App\Bases\BaseRequest;

/**
 * Class CreateUserRequest
 * @package App\Http\Requests\User
 */
class UpdateUserRequest extends BaseRequest{

    /**
     * @description toggle authorization rules
     *
     * @return bool
     */
    public function authorize(){
        // TODO: Implement authorize() method.
        return true;
    }

    /**
     * @description validation rules
     *
     * @return array
     */
    public function rules(){
        // TODO: Implement rules() method.
        return [
            "name" => "min:10|max:255",
            "email" => "email|min:10|max:255|unique:user",
            "password" => "min:8|max:30"
        ];
    }

    /**
     * @description validation failed messages
     *
     * @return array
     */
    public function messages(){
        return [
            "name.min" => "Nome deve conter mais de 10 caracteres",
            "name.max" => "Nome deve conter menos de 255 caracteres",
            "email.email" => "E-mail inválido",
            "email.unique" => "E-mail já existe em nossa base de dados",
            "email.min" => "E-mail deve conter mais de 10 caracteres",
            "email.max" => "Email deve conter menos de 255 caracteres",
            "password.min" => "Senha de acesso deve conter mais de 8 caracteres",
            "password.max" => "Senha de acesso deve conter menos de 30 caracteres"
        ];
    }
}