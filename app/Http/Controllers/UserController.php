<?php
/**
 * Created by PhpStorm.
 * User: wilbe_thaae2h
 * Date: 25/01/2019
 * Time: 11:57
 */

namespace App\Http\Controllers;

use App\Bases\BaseController;
use App\Http\Requests\User\CreateUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Services\UserService;
use Illuminate\Http\Request;

/**
 * Class UserController
 * @package App\Http
 */
class UserController extends BaseController {

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function list(Request $request){
        $userList = UserService::list($request->all());
        $this->setResponseStatusCode( HTTP_STATUS_SUCCESS );
        $this->setResponseMessage( count($userList) ? "" : "Nada Entrado" );
        $this->setResponseBody( $userList );
        return $this->jsonResponse();
    }

    /**
     * @param CreateUserRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    function create(CreateUserRequest $request){
        $createServiceResponse = UserService::insert( (object) $request->all() );
        $this->setResponseStatusCode( $createServiceResponse->statusCode );
        $this->setResponseBody( $createServiceResponse->data );
        return $this->jsonResponse();
    }
    function update(UpdateUserRequest $request, int $id){
        $userService = UserService::find( $id );

        if(!$userService)
            $this->setResponseStatusCode( HTTP_STATUS_SUCCESS );
        else
            $this->setResponseStatusCode( $userService->update( (object) $request->all() )->statusCode );

        return $this->jsonResponse();
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    function delete(int $id){
        $userService = UserService::find($id);

        if(!$userService)
            $this->setResponseStatusCode( HTTP_STATUS_SUCCESS );
        else
            $this->setResponseStatusCode( $userService->delete()->statusCode );

        return $this->jsonResponse();
    }
}