<?php

namespace App\Bases;

use App\Bases\BaseRepository;
use Illuminate\Http\UploadedFile;

/**
 * Class BaseService
 * @package App\Services
 *
 * @author Wilber da Silva Chaves Costa <wilber.silva@4move.me>
 */
class BaseService{

    /**
     * @var BaseRepository
     */
    protected $repository;

    /**
     * BaseService constructor.
     * @param BaseRepository $repository
     */
    public function __construct(BaseRepository $repository){
        $this->repository = $repository;
    }

    /**
     * @param UploadedFile $file
     * @param array $bucket
     * @param string $customerName
     * @return object|string
     */
    static function uploadFile(UploadedFile $file, Array $bucket, $customerName = ""){

    }

    /**
     * @param string $path
     * @param array $bucket
     * @return object
     */
    static function deleteFile(string $path, array $bucket){

    }

    /**
     * @description Delete into database (Using softDeletes)
     *
     * @return object
     */
    function delete(){

        if(!$this->repository->delete()) return $this::handle(HTTP_INTERNAL_SERVER_ERROR);

        return $this::handle(HTTP_STATUS_SUCCESS);
    }

    /**
     * @param int $statusCode
     * @param string|null $message
     * @param mixed $data
     * @return object
     */
    static function handle(int $statusCode, $data = [], ?string $message = null){
        return handle($statusCode, $data, $message);
    }
}