<?php

namespace App\Bases;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

/**
 * Class BaseController
 * @package App\Http\Controllers\Base
 */
class BaseController extends Controller{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @var array *** defaults headers json response
     */
    private $headerJsonResponse = [
        "Content-Type:application/x-www-form-urlencoded"
    ];

    /**
     * @var int
     */
    private $responseStatusCode = HTTP_STATUS_SUCCESS;
    /**
     * @var string
     */
    protected $responseMessage = null;

    /**
     * @var array
     */
    protected $responseBody = [];

    /**
     * @param int $statusCode
     * @return void
     */
    protected function setResponseStatusCode(int $statusCode){
        $this->responseStatusCode = $statusCode;
    }

    /**
     * @param string $message
     */
    protected function setResponseMessage(string $message){
        $this->responseMessage = $message;
    }

    /**
     * @param $body
     */
    protected function setResponseBody($body){
        $this->responseBody = $body;
    }


    /**
     * @param array $messages
     * @return \Illuminate\Http\JsonResponse
     */
    protected function jsonResponse(array $messages = []){
        // must use another response methods after json() method
        $data = [
            "data" => $this->responseBody
        ];
        if(count($messages)) $data["messages"] = $messages;

        if(!$this->responseMessage)
            switch ($this->responseStatusCode){
                case HTTP_STATUS_SUCCESS:
                    $data["message"] = "Operação Realizada com Sucesso";
                    break;
                case HTTP_STATUS_NOT_FOUND :
                    $data["message"] = "Não encontrado";
                    break;
                default:
                    $data["message"] = "Houve um erro interno";
                    break;
            }
        else
            $data["message"] = $this->responseMessage;

        return response()
                ->json($data) // use json_encode()
                ->setStatusCode($this->responseStatusCode)
                ->withHeaders($this->headerJsonResponse);
    }


}
