<?php

namespace App\Bases;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Model
 * @package App\Models
 *
 * **************************************************
 * ********* Default rules for every models *********
 * **************************************************
 *
 * @description BaseModel have defaults configs tables
 *
 * @author Wilber da Silva Chaves Costa <wilber.silva@4move.me>
 */

class BaseModel extends Eloquent{

    use SoftDeletes;

    /**
     * The default primary key
     *
     * @var string
     */
    protected $primaryKey = "id";

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];


    /**
     * @return int
     */
    function getId(){
        return $this->getter("id");
    }

    /**
     * @param $format
     * @return false|string
     */
    function getCreatedAt($format){
        return $this->getter('created_at')->format($format);
    }

    /**
     * @param $format
     * @return false|string
     */
    function getUpdatedAt($format){
        return $this->getter('created_at')->format($format);
    }

    /**
     * @param $format
     * @return false|string
     */
    function getDeletedAt($format){
        return ($this->getter('deleted_at') ? $this->getter('deleted_at')->format($format) : null);
    }

    /**
     * @param string $property
     * @return mixed
     */
    function getter($property){
        return $this->$property;
    }

    /**
     * @param $property
     * @param mixed $value
     */
    function setter($property, $value){
        $this->$property = $value;
    }

}