<?php

namespace App\Bases;

use App\Bases\BaseModel;

/**
 * Class BaseRepository
 * @package App\Repositories
 *
 * @description BaseRepository have general Repositories methods
 *
 * @author Wilber da Silva Chaves Costa <wilber.silva@4move.me>
 */
abstract class BaseRepository{

    /**
     * @var string
     */
    protected static $model = BaseModel::class;

    /**
     * @var BaseModel
     */
    protected $dataObject;

    /**
     * @var array
     */
    protected $originalDate;

    /**
     * @var int
     */
    protected static $paginate = 30;

    /**
     * BaseRepository constructor.
     *
     * @description super::class constructor
     *
     * @param BaseModel $modelObject = You can be use BaseModelSon::class
     */
    protected function __construct(BaseModel $modelObject){
        $this->dataObject = $modelObject;
//        $this->originalDate = $this->dataObject->getOriginal();
    }

    /**
     * @param object $data
     * @return $this|null
     */
    abstract static function insert($data);

    /**
     * @param object $data
     * @return bool
     */
    abstract function update($data);

    function save(){
        $this->dataObject->save();
    }

    /**
     * @description delete current dataObject
     *
     * @return bool
     */
    function delete(){
        try{
            $this->dataObject->delete();

            return true;
        }catch (\Exception $e){
            return false;
        }
    }

    /**
     * @param boolean $success
     * @param integer $code
     * @param string|array $messages
     * @return object
     */
    protected static function handle($success, $code = 200, $messages){
        return handle($success, $code, $messages);
    }

}