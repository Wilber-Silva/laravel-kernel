<?php

namespace App\Contracts;

use App\Bases\BaseModel;
use App\Bases\BaseRepository;
use Illuminate\Support\Collection;

/**
 * Interface Repository
 * @package App\Repositories\Interfaces
 *
 * @author Wilber da Silva Chaves Costa <wilber.silva@4move.me>
 */
interface RepositoryContract{

    /**
     * @description Search the model by id and return current repository
     *
     * @param int $id
     *
     * @return $this|null
     */
    static function find(?int $id);

    /**
     * @param object $config
     * @return Collection
     */
    static function list($config);

    /**
     * @description Insert date into data base
     *
     * @param object $inputs
     * @return bool|$this
     */
    static function insert($inputs);

    /**
     * @description get the represented data repository
     *
     * @return BaseModel
     */
    function getDataObject();

    /**
     * @description save represented data values (overwrite super::class method)
     *
     * @param object $inputs
     *
     * @return bool
     */
    function update($inputs);
}