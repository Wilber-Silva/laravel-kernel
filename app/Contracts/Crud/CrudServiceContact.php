<?php

namespace App\Contracts\Crud;

use Illuminate\Support\Collection;

/**
 * Interface Crud
 * @package App\Services\Interfaces
 *
 * @author Wilber da Silva Chaves Costa <wilber.silva@4move.me>
 */
interface CrudServiceContact{

    /**
     * @description find in the database 
     *
     * @param int $id
     * @return $this|null
     */
    static function find(?int $id);

    /**
     * @param array $config
     * @return Collection
     */
    static function list(array $config);

    /**
     * @description Inset into database
     *
     * @param object $inputs
     *
     * @return object
     */
    static function insert($inputs);

    /**
     * @description Update into database
     *
     * @param object $inputs
     *
     * @return object
     */
     function update($inputs);

    /**
     * @description Delete into database (Using softDeletes)
     *
     * @return object
     */
    function delete();
}