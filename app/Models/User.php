<?php

namespace App\Models;

use App\Bases\BaseModel;

/**
 * Class User
 * @package App
 */
class User extends BaseModel {
    protected $table = "user";
}
