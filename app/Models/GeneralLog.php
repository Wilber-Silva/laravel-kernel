<?php
/**
 * Created by PhpStorm.
 * User: wilbe_thaae2h
 * Date: 07/12/2018
 * Time: 11:14
 */

namespace App\Models;

use App\Bases\BaseModel;

/**
 * Class GeneralLog
 * @package App\Models
 */
class GeneralLog extends BaseModel{

    protected $table = "general_log";
}