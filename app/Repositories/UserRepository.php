<?php
/**
 * Created by PhpStorm.
 * User: wilbe_thaae2h
 * Date: 25/01/2019
 * Time: 12:10
 */
namespace App\Repositories;

use App\Bases\BaseRepository;
use App\Bases\BaseModel;
use App\Contracts\RepositoryContract;
use App\Models\User;
use Illuminate\Support\Collection;

/**
 * Class UserRepository
 * @package App\Repositories
 */
class UserRepository extends BaseRepository  implements RepositoryContract {

    /**
     * UserRepository constructor.
     * @param User $modelObject
     */
    protected function __construct(User $modelObject){
        parent::__construct($modelObject);
    }

    /**
     * @param object $data
     * @return $this|null
     */
    static function insert($data){
        // TODO: Implement insert() method.

        try{
            $user = new User();
            $user->setter( "name", $data->name );
            $user->setter( "email", $data->email );
            $user->setter( "password", $data->password );
            $user->save();

            return new self($user);
        }catch (\Exception $e){
            return null;
        }
    }

    /**
     * @param object $data
     * @return bool
     */
    function update($data){
        // TODO: Implement update() method.

        try{
            if(isset($data->name))      $this->dataObject->setter( "name", $data->name );
            if(isset($data->email))     $this->dataObject->setter( "email", $data->email );
            if(isset($data->password))  $this->dataObject->setter( "password", $data->password );
            $this->dataObject->save();

            return true;

        }catch (\Exception $e){
            return false;
        }
    }

    /**
     * @description Search the model by id and return current repository
     *
     * @param int $id
     *
     * @return $this|null
     */
    static function find(?int $id){
        // TODO: Implement find() method.
        $user = User::find($id);

        if(!$user) return null;

        return new self($user);
    }

    /**
     * @param object $config
     * @return Collection
     */
    static function list($config){
        // TODO: Implement list() method.
        return User::where(function ($query) use ($config){
                if(!empty($config->id)) $query->where("id", "=", $config->id);
                if(!empty($config->name)) $query->where("name", "=", $config->name);
                if(!empty($config->email)) $query->where("email", "=", $config->email);
            })
            ->orderBy("id", $config->idOrder)
            ->orderBy("name", $config->nameOrder)
            ->orderBy("email", $config->emailOrder)
            ->select(
                "id",
                "name",
                "email",
                "created_at",
                "updated_at"
            )
            ->get();
    }

    /**
     * @description get the represented data repository
     *
     * @return BaseModel|User
     */
    function getDataObject(){
        // TODO: Implement getDataObject() method.
        return $this->dataObject;
    }
}