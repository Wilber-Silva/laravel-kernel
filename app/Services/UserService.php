<?php
/**
 * Created by PhpStorm.
 * User: wilbe_thaae2h
 * Date: 25/01/2019
 * Time: 12:21
 */

namespace App\Services;
use App\Bases\BaseService;
use App\Bases\BaseRepository;
use App\Contracts\Crud\CrudServiceContact;
use App\Repositories\UserRepository;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;

/**
 * Class UserService
 * @package App\Services
 */
class UserService extends BaseService implements CrudServiceContact {

    /**
     * UserService constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository){
        parent::__construct( $repository );
    }

    /**
     * @description find in the database
     *
     * @param int $id
     * @return $this|null
     */
    static function find(?int $id){
        // TODO: Implement find() method.
        $repository = UserRepository::find( $id );

        if(!$repository) return null;

        return new self( $repository );
    }

    /**
     * @param array $config
     * @return Collection
     */
    static function list(array $config){
        // TODO: Implement list() method.
        return UserRepository::list( (object) [
            "id"            => isset( $config["id"] ) ? $config["id"] : "",
            "name"          => isset( $config["name"] ) ? $config["name"] : "",
            "email"         => isset( $config["email"] ) ? $config["email"] : "",
            "idOrder"       => isset( $config["idOrder"] ) ? $config["idOrder"] : "DESC",
            "nameOrder"     => isset( $config["nameOrder"] ) ? $config["nameOrder"] : "ASC",
            "emailOrder"    => isset( $config["emailOrder"] ) ? $config["emailOrder"] : "ASC",
        ] );
    }

    /**
     * @description Inset into database
     *
     * @param object $inputs
     *
     * @return object
     */
    static function insert($inputs){
        // TODO: Implement insert() method.
        $inputs->password = Hash::make( $inputs->password );
        $repository = UserRepository::insert( $inputs );

        if(!$repository) return self::handle( HTTP_INTERNAL_SERVER_ERROR );

        return self::handle( HTTP_STATUS_SUCCESS, $repository->getDataObject() );
    }

    /**
     * @description Update into database
     *
     * @param object $inputs
     *
     * @return object
     */
    function update($inputs){
        // TODO: Implement update() method.
        if( !$this->repository->update( $inputs ) ) return self::handle( HTTP_INTERNAL_SERVER_ERROR );

        return self::handle( HTTP_STATUS_SUCCESS );
    }
}