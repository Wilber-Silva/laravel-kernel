<?php

namespace App\Observers;

use App\Bases\BaseModel;
use App\Models\GeneralLog;

/**
 * Class BaseModelObserver
 * @package App\Observers
 */
class BaseModelObserver{

    const INSERT_OPERATION = 'INSERT';
    const UPDATE_OPERATION = 'UPDATE';
    const DELETE_OPERATION = 'DELETE';

    /**
     * Handle the base model "created" event.
     *
     * @param  BaseModel  $baseModel
     * @return void
     */
    function created(BaseModel $baseModel){
        try{
            $log = new GeneralLog();
            $log->setter("origin_table", $baseModel->getTable());
            $log->setter("owner_id", 1);
            $log->setter("owner_account_type", "ADMIN");
            $log->setter("type_operation", self::INSERT_OPERATION);
            $log->setter("log", "");
            $log->save();
        }catch (\Exception $e){dd($e->getMessage());}
    }

    /**
     * Handle the base model "updated" event.
     *
     * @param  BaseModel  $baseModel
     * @return void
     */
    function updated(BaseModel $baseModel){
        try{
            $log = new GeneralLog();
            $log->setter("origin_table", $baseModel->getTable());
            $log->setter("owner_id", 1);
            $log->setter("owner_account_type", "ADMIN");
            $log->setter("type_operation", self::UPDATE_OPERATION);
            $log->setter("log", json_encode((object) [
                "original" => $baseModel->getOriginal(),
                "changes" => $baseModel->getChanges()
            ]));
            $log->save();
        }catch (\Exception $e){dd($e->getMessage());}
    }

    /**
     * Handle the base model "saved" event.
     *
     * @param  BaseModel  $baseModel
     * @return void
     */
    function saved(BaseModel $baseModel){
    }

    /**
     * Handle the base model "deleted" event.
     *
     * @param  BaseModel  $baseModel
     * @return void
     */
    function deleted(BaseModel $baseModel){
        try{
            $log = new GeneralLog();
            $log->setter("origin_table", $baseModel->getTable());
            $log->setter("owner_id", 1);
            $log->setter("owner_account_type", "ADMIN");
            $log->setter("type_operation", self::DELETE_OPERATION);
            $log->setter("log", json_encode((object) [
                "original" => $baseModel->getOriginal()
            ]));
            $log->save();
        }catch (\Exception $e){}
    }

    /**
     * Handle the base model "restored" event.
     *
     * @param  BaseModel  $baseModel
     * @return void
     */
    function restored(BaseModel $baseModel)
    {
        //
    }

    /**
     * Handle the base model "force deleted" event.
     *
     * @param  BaseModel  $baseModel
     * @return void
     */
    function forceDeleted(BaseModel $baseModel)
    {
        //
    }
}
