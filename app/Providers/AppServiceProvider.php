<?php

namespace App\Providers;

use App\Models\User;
use App\Observers\BaseModelObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(){
        User::observe(BaseModelObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(){
        //
    }
}
